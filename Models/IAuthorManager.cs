﻿using System.Collections.Generic;

namespace Models
{
    public interface IAuthorManager
    {
        void Initialize();
        List<Author> GetAuthorCollection();
        Author GetAuthor(int authorId);
        Author InsertAuthor(Author author);
        bool DeleteAuthor(int authorId);
        Author UpdateAuthor(Author newAuthor);

        bool DeleteBook(int deletedBookId);
        Book GetBook(int bookId);
        void InsertBook(Book book);
        Book UpdateBook(Book newBook);

        void AddNewShelfToBook(Shelf shelf, Book book);
        void RemoveShelfFromBook(Shelf shelf, Book book);

        List<Shelf> GetShelfsCollection();
        void InsertShelf(Shelf shelf);
        bool DeleteShelf(int shelfId);
        Shelf GetShelf(int shelfId);
        Shelf UpdateShelf(Shelf shelf);

        List<Shelf> GetShelfsCollectionByBookId(int bookId);
        List<Book> GetBooksCollectionByShelfId(int shelfId);

    }
}
