﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models
{
    public class AuthorManager : IAuthorManager
    {
        public AuthorContext authorContext;

        public AuthorManager()
        {
            authorContext = new AuthorContext();
            Initialize();
        }
        public AuthorManager(AuthorContext context)
        {
            authorContext = context;
            Initialize();
        }

        public List<Author> GetAuthorCollection()
        {
            var authorsCollection = authorContext.Authors.Include(a => a.Books).ToList();
            return authorsCollection;
        }

        public Author GetAuthor(int authorId)
        {
            var author = authorContext.Authors.Include(a => a.Books).FirstOrDefault(a => a.Id == authorId);
            return author;
        }

        public Author InsertAuthor(Author author)
        {
            authorContext.Authors.Add(author);
            authorContext.SaveChanges();
            return author;
        }

        public bool DeleteAuthor(int authorId)
        {
            var author = GetAuthor(authorId);
            if (author != null)
            {
                authorContext.Remove(author);
                authorContext.SaveChanges();
                return true;
            }
            return false;
        }

        public Author UpdateAuthor(Author author)
        {
            authorContext.Update(author);
            authorContext.SaveChanges();
            return author;
        }


        public bool DeleteBook(int deletedBookId)
        {
            var book = GetBook(deletedBookId);
            if (book != null)
            {
                authorContext.Remove(book);
                authorContext.SaveChanges();
                return true;
            }
            return false;
        }

        public Book GetBook(int bookId)
        {
            var book = authorContext.Books.Include(b => b.Author).FirstOrDefault(b => b.Id == bookId);
            return book;
        }

        public void InsertBook(Book book)
        {
            authorContext.Books.Add(book);
            authorContext.SaveChanges();
        }

        public Book UpdateBook(Book book)
        {
            var newBook = GetBook(book.Id);
            if (newBook != null)
            {
                newBook.Title = book.Title;
                authorContext.Update(newBook);
                authorContext.SaveChanges();
                return authorContext.Books.FirstOrDefault(b => b.Id == newBook.Id);
            }
            return null;
        }


        public List<Shelf> GetShelfsCollection()
        {
            return authorContext.Shelfs.ToList();
        }

        public void InsertShelf(Shelf shelf)
        {
            authorContext.Add(shelf);
            authorContext.SaveChanges();
        }

        public bool DeleteShelf(int shelfId)
        {
            var shelf = authorContext.Shelfs.FirstOrDefault(s => s.Id == shelfId);
            if (shelf != null)
            {
                authorContext.Remove(shelf);
                authorContext.SaveChanges();
                return true;
            }
            return false;
        }

        public Shelf GetShelf(int shelfId)
        {
            return authorContext.Shelfs.FirstOrDefault(s => s.Id == shelfId);
        }

        public Shelf UpdateShelf(Shelf shelf)
        {
            shelf = authorContext.Shelfs.FirstOrDefault(s => s.Id == shelf.Id);
            if (shelf != null)
            {
                authorContext.Update(shelf);
                authorContext.SaveChanges();
            }
            return authorContext.Shelfs.FirstOrDefault(s => s.Id == shelf.Id);
        }


        public List<Shelf> GetShelfsCollectionByBookId(int bookId)
        {
            var book = authorContext.Books.Include(b => b.ShelfsBooks).ThenInclude(sb => sb.Shelf).FirstOrDefault(b => b.Id == bookId);
            var shelfs = new List<Shelf>();
            foreach (var sb in book.ShelfsBooks)
            {
                shelfs.Add(sb.Shelf);
            }
            foreach (var s in shelfs)
            {
                s.ShelfsBooks = null;
            }
            return shelfs;
        }
        public List<Book> GetBooksCollectionByShelfId(int shelfId)
        {
            var shelf = authorContext.Shelfs.Include(b => b.ShelfsBooks).ThenInclude(sb => sb.Book).FirstOrDefault(s => s.Id == shelfId);
            var books = new List<Book>();
            foreach (var sb in shelf.ShelfsBooks)
            {
                books.Add(sb.Book);
            }
            foreach (var b in books)
            {
                b.ShelfsBooks = null;
            }
            return books;
        }

        public void AddNewShelfToBook(Shelf shelf, Book book)
        {
            book = GetBook(book.Id);
            book.ShelfsBooks.Add(new ShelfsBooks() { ShelfId = shelf.Id, BookId = book.Id });
            authorContext.Update(book);
            authorContext.SaveChanges();
        }

        public void RemoveShelfFromBook(Shelf shelf, Book book)
        {
            shelf = authorContext.Shelfs.Include(s => s.ShelfsBooks).FirstOrDefault(s => s.Id == shelf.Id);
            book = authorContext.Books.Include(b => b.ShelfsBooks).FirstOrDefault(b => b.Id == book.Id);
            if (book != null && shelf != null)
            {
                var shelfsBooks = shelf.ShelfsBooks.FirstOrDefault(sb => sb.BookId == book.Id);
                shelf.ShelfsBooks.Remove(shelfsBooks);
                authorContext.SaveChanges();
            }
        }


        public void Initialize()
        {
            if (!authorContext.Authors.Any())
            {
                var Rudolf = new Author()
                {
                    FirstName = "Rudolf",
                    LastName = "Berghammer",
                    Books = new List<Book>()
                };
                Rudolf.Books.AddRange(new List<Book>
                {
                     new Book(){Title="Semantik von Programmiersprachen",Author =Rudolf},
                     new Book(){Title= "Ordnungen", Author = Rudolf },
                     new Book(){Title = "Mathematik für Informatiker: Grundlegende Begriffe und Strukturen", Author = Rudolf }
                    });

                var Nikil = new Author()
                {
                    FirstName = "Nikil",
                    LastName = "Dutt",
                    Books = new List<Book>()

                };
                Nikil.Books.AddRange(new List<Book> { new Book() { Title = "Memory Issues in Embedded Systems-on-Chip: Optimizations and Exploration", Author = Nikil } });

                var Taras = new Author()
                {
                    FirstName = "Taras",
                    LastName = "Butenko",
                    Books = new List<Book>()
                };
                Taras.Books.AddRange(new List<Book> { new Book() { Title = "Some interesting stories", Author = Taras } });

                var John = new Author()
                {
                    FirstName = "John",
                    LastName = "Lennon",
                    Books = new List<Book>()
                };
                John.Books.AddRange(new List<Book>() { new Book() { Title = "Yesterday", Author = John } });

                authorContext.AddRange(new List<Author> { Rudolf, Nikil, Taras, John });
                authorContext.SaveChanges();
            }
        }
    }
}