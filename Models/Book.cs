﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Book
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Title { get; set; }
        
        public int AuthorId { get; set; }

        public Author Author { get; set; }

        public List<ShelfsBooks> ShelfsBooks { get; set; }

        public Book()
        {
            ShelfsBooks = new List<ShelfsBooks>();
        }
    }
}