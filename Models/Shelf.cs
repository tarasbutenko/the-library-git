﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Shelf
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public List<ShelfsBooks> ShelfsBooks { get; set; }
        public Shelf()
        {
            ShelfsBooks = new List<ShelfsBooks>();
        }
    }
}
