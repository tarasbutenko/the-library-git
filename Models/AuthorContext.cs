﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AuthorContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Shelf> Shelfs { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=thelibrarybutenko.database.windows.net;Database=thelibrarydb;User Id=butenko; Password = pdapda1ajm/;");// Trusted_Connection =False;Encrypt=True;");
          //  optionsBuilder.UseSqlServer("Server=(LocalDb)\\mssqllocaldb;Database=thelibrarydb;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                .HasMany(p => p.Books)
                .WithOne(t => t.Author)
                .OnDelete(DeleteBehavior.Cascade);
                
            //modelBuilder.Entity<Book>().HasOne(b=>b.Author).WithMany(a=>a.Books).HasForeignKey(a=>a.AuthorId);


            modelBuilder.Entity<ShelfsBooks>()
            .HasKey(t => new { t.BookId, t.ShelfId });

            modelBuilder.Entity<ShelfsBooks>()
                .HasOne(sb => sb.Book)
                .WithMany(b => b.ShelfsBooks)
                .HasForeignKey(sb => sb.BookId);

            modelBuilder.Entity<ShelfsBooks>()
                .HasOne(sb => sb.Shelf)
                .WithMany(s => s.ShelfsBooks)
                .HasForeignKey(sc => sc.ShelfId);
        }
    }
}
