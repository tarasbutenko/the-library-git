﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AuthorManagerOld : IAuthorManager
    {
        public static List<Author> AuthorsCollection = new List<Author>
            {
             new Author(){FirstName="Rudolf", LastName="Berghammer",Id=1, Books= new List<Book>
             {
                new Book(){Title="Semantik von Programmiersprachen",Id = 1 },
                new Book(){Title= "Ordnungen",Id = 2 },
                new Book(){Title = "Mathematik für Informatiker: Grundlegende Begriffe und Strukturen",Id = 3 }
             } },
             new Author(){FirstName = "Nikil", LastName= "Dutt",Id = 2, Books = new List<Book>
             {
                new Book(){Title  = "Memory Issues in Embedded Systems-on-Chip: Optimizations and Exploration",Id=4}
             } },
             new Author(){FirstName = "Taras", LastName = "Butenko",Id= 3,Books =new List<Book>{
                new Book(){Title = "Some interesting stories",Id = 5 } } },
             new Author(){FirstName = "John", LastName = "Lennon",Id = 4,Books = new List<Book>{
                new Book(){Title = "Yesterday",Id = 6 } } }
          };

        public AuthorManagerOld()
        {

        }

        public List<Author> GetAuthorCollection()
        {
            return AuthorsCollection;
        }

        public Author GetAuthorByBookId(int bookId)
        {
            return GetAuthorCollection().FirstOrDefault(a => GetBooksCollectionByAuthor(a).Any(book => book.Id == bookId));
        }

        public Author GetAuthor(int authorId)
        {
            return GetAuthorCollection().FirstOrDefault(a => a.Id == authorId);
        }

        public Author InsertAuthor(Author author)
        {
            var newAuthor = new Author() { FirstName = author.FirstName, LastName = author.LastName, Id = ReturnNewAuthorId() };
            AuthorsCollection.Add(newAuthor);
            return newAuthor;
        }

        public bool DeleteAuthor(int authorId)
        {
            return AuthorsCollection.Remove(GetAuthor(authorId));
        }

        internal int ReturnNewAuthorId()
        {
            int newAuthorId = AuthorsCollection.Last().Id;
            do
            {
                newAuthorId++;
            } while (AuthorsCollection.Any(a => a.Id == newAuthorId));
            return newAuthorId;
        }


        public Book GetNewBook(string title)
        {

            return new Book() { Title = title, Id = ReturnNewBookId() };
        }

        public int ReturnNewBookId()
        {
            var booksCollection = GetBooksCollection();
            int newBookId = booksCollection.Last().Id;
            do
            {
                newBookId++;
            } while (booksCollection.Any(book => book.Id == newBookId));
            return newBookId;
        }

        public List<Book> GetBooksCollection()
        {
            var BooksCollection = new List<Book>();
            foreach (var author in AuthorsCollection)
            {
                BooksCollection.AddRange(GetBooksCollectionByAuthor(author));
            }
            return BooksCollection;
        }

        public bool DeleteBook(int deletedBookId)
        {
            if (GetBooksCollection().Any(b => b.Id == deletedBookId))
            {
                var author = GetAuthorByBookId(deletedBookId);
                return author.Books.Remove(GetBook(deletedBookId));
            }
            else
            {
                return false;
            }
        }

        public Book GetBook(int bookId)
        {
            return GetBooksCollection().FirstOrDefault(book => book.Id == bookId);
        }

        public void InsertBook( Book book)
        {
            var author = GetAuthor(book.AuthorId);
            GetBooksCollectionByAuthor(author).Add(book);
        }

        public List<Book> GetBooksCollectionByAuthor(Author author)
        {
            return author.Books;
        }

        public Author UpdateAuthor(Author newAuthor)
        {
            throw new NotImplementedException();
        }

        public Book UpdateBook(Book newBook)
        {
            throw new NotImplementedException();
        }

        public void Initialize()
        {
        }

        public List<Shelf> GetShelfsCollection()
        {
            throw new NotImplementedException();
        }

        public void InsertShelf(Shelf shelf)
        {
            throw new NotImplementedException();
        }

        public Shelf GetShelf(int shelfId)
        {
            throw new NotImplementedException();
        }

        public Shelf UpdateShelf(Shelf shelf)
        {
            throw new NotImplementedException();
        }


        public void AddNewShelfToBook(Shelf shelf, Book book)
        {
            throw new NotImplementedException();
        }

        public void RemoveShelfFromBook(Shelf shelf, Book book)
        {
            throw new NotImplementedException();
        }

        public List<Shelf> GetShelfsCollectionByBookId(int bookId)
        {
            throw new NotImplementedException();
        }

        public List<Book> GetBooksCollectionByShelfId(int shelfId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteShelf(int shelfId)
        {
            throw new NotImplementedException();
        }
    }
}
