﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ShelfsBooks
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int ShelfId { get; set; }
        public Shelf Shelf { get; set; }
        public ShelfsBooks()
        { }

    }
}
