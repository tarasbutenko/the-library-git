﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models;

namespace TheLibrariApiTest
{
    public class FakeAuthorManager : IAuthorManager
    {
        private List<Shelf> shelfsCollection = new List<Shelf>
        {
            new Shelf() { Id=1,Name="fake shelf 1"},
            new Shelf() { Id=2,Name="fake shelf 2"},
        };
        public List<Author> AuthorsCollection = new List<Author>
            {
             new Author(){FirstName = "Fake", LastName= "Author 1", Id=1,Books = new List<Book>
             {
                new Book(){Title = "fake title 1", Id = 1 ,AuthorId=1},
                new Book(){Title = "fake title 2",Id= 2,AuthorId=1},
                new Book(){Title= "fake title 3",Id= 3,AuthorId=1} }
                },
             new Author(){FirstName = "Fake", LastName= "Author 2", Id=2,Books = new List<Book>
             {
                new Book(){Title = "fake title 4", Id = 4,AuthorId=2 }
             } },
             new Author(){FirstName = "Fake", LastName= "Author 3", Id=3,Books = new List<Book>
             {
                new Book(){Title = "fake title 5", Id = 5 ,AuthorId=3}
             } },
             new Author(){FirstName = "Fake", LastName= "Author 4", Id=4,Books = new List<Book>
             {
                new Book(){Title = "fake title 6", Id = 6 }
             } }
          };

        public FakeAuthorManager()
        {

        }

        public List<Author> GetAuthorCollection()
        {
            return AuthorsCollection;
        }

        public Author GetAuthorByBookId(int bookId)
        {
            return GetAuthorCollection().FirstOrDefault(a => GetBooksCollectionByAuthor(a).Any(book => book.Id == bookId));
        }

        public Author GetAuthor(int authorId)
        {
            return GetAuthorCollection().FirstOrDefault(a => a.Id == authorId);
        }

        public Author InsertAuthor(Author author)
        {
            var newAuthor = new Author() { FirstName = author.FirstName, LastName = author.LastName, Id = ReturnNewAuthorId() };
            AuthorsCollection.Add(newAuthor);
            return newAuthor;
        }

        public bool DeleteAuthor(int authorId)
        {
            return AuthorsCollection.Remove(GetAuthor(authorId));
        }

        internal int ReturnNewAuthorId()
        {
            int newAuthorId = AuthorsCollection.Last().Id;
            do
            {
                newAuthorId++;
            } while (AuthorsCollection.Any(a => a.Id == newAuthorId));
            return newAuthorId;
        }


        public Book GetNewBook(string title)
        {

            return new Book() { Title = title, Id = ReturnNewBookId() };
        }

        public int ReturnNewBookId()
        {
            var booksCollection = GetBooksCollection();
            int newBookId = booksCollection.Last().Id;
            do
            {
                newBookId++;
            } while (booksCollection.Any(book => book.Id == newBookId));
            return newBookId;
        }

        public List<Book> GetBooksCollection()
        {
            var BooksCollection = new List<Book>();

            foreach (var author in AuthorsCollection)
            {
                BooksCollection.AddRange(GetBooksCollectionByAuthor(author));
            }
            return BooksCollection;
        }

        public bool DeleteBook(int deletedBookId)
        {
            if (deletedBookId > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Book GetBook(int bookId)
        {
            return GetBooksCollection().FirstOrDefault(book => book.Id == bookId);
        }

        public void InsertBook(Book book)
        {
            var author = GetAuthor(book.AuthorId);
            GetBooksCollectionByAuthor(author).Add(book);
        }

        public List<Book> GetBooksCollectionByAuthor(Author author)
        {
            return author.Books;
        }

        public Author UpdateAuthor(Author newAuthor)
        {
            var author = GetAuthor(newAuthor.Id);
            author.LastName = newAuthor.LastName;
            author.FirstName = newAuthor.FirstName;
            return author;
        }

        public Book UpdateBook(Book newBook)
        {
            var book = GetBook(newBook.Id);
            book.Title = newBook.Title;
            return book;
        }

        public void Initialize()
        { }

        public List<Shelf> GetShelfsCollection()
        {
            return shelfsCollection;
        }

        public void InsertShelf(Shelf shelf)
        {
            shelfsCollection.Add(shelf);
        }


        public Shelf GetShelf(int shelfId)
        {
           return shelfsCollection.FirstOrDefault(s=>s.Id==shelfId);
        }

        public Shelf UpdateShelf(Shelf shelf)
        {
            var newShelf = shelfsCollection.FirstOrDefault(s => s.Id == shelf.Id);
            newShelf.Name=shelf.Name;
            return newShelf;
        }


        public void AddNewShelfToBook(Shelf shelf, Book book)
        {
        }

        public void RemoveShelfFromBook(Shelf shelf, Book book)
        {
        }

        public List<Shelf> GetShelfsCollectionByBookId(int bookId)
        {
            return GetShelfsCollection();
        }

        public List<Book> GetBooksCollectionByShelfId(int shelfId)
        {
            return GetBooksCollection();
        }

        public bool DeleteShelf(int shelfId)
        {
           return shelfsCollection.Remove(GetShelf(shelfId));
        }
    }
}