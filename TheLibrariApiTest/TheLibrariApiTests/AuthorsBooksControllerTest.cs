﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheLibraryApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Models;
using System.Net;

namespace TheLibrariApiTest
{
    [TestClass]
    public class AuthorsBooksControllerTest
    {
        private IAuthorManager fakeAuthorManager;
        private AuthorsBooksController controller;

        [TestInitialize]
        public void StartUp()
        {
            fakeAuthorManager = new FakeAuthorManager();
            controller = new AuthorsBooksController(fakeAuthorManager);
        }

        [TestMethod]
        [DataRow(1)]
        public void Get_ValidAuthorId_OkObjectResultWithBooksCollection(int authorId)
        {
            var expected = fakeAuthorManager.GetAuthor(authorId).Books;
            var expectedObject = new JsonResult(expected);

            var result = (JsonResult)controller.Get(authorId);

            Assert.AreEqual(expectedObject.Value, (result).Value);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(100)]
        public void Get_InvalidAuthorId_NotFoundResult(int invalidAuthorId)
        {
            var expectedStatusCode = (int)HttpStatusCode.NotFound; ;

            var result = (NotFoundResult)controller.Get(invalidAuthorId);
            var actualSatusCode = result.StatusCode;
            Assert.AreEqual(expectedStatusCode, actualSatusCode);
        }

        [TestMethod]
        [DataRow(2, -1)]
        [DataRow(2, 0)]
        [DataRow(2, 100)]
        public void Get_ValidAuthorIdAndInvalidBookId_NotFoundResult(int authorId, int invalidBookId)
        {
            var expected = fakeAuthorManager.GetAuthor(authorId);
            var expectedStatusCode = (int)HttpStatusCode.NotFound; ;

            var result = (NotFoundResult)controller.Get(invalidBookId, authorId);
            var actualSatusCode = result.StatusCode;
            Assert.AreEqual(expectedStatusCode, actualSatusCode);
        }

        [TestMethod]
        [DataRow(2, 4)]
        public void Get_ValidAuthorIdAndValidBookId_OkObjectResultWithBook(int authorId, int bookId)
        {
            var expected = fakeAuthorManager.GetBook(bookId);

            var result = (JsonResult)controller.Get(bookId, authorId);

            Assert.AreEqual(expected, (result).Value);
        }

        [TestMethod]
        [DataRow("Title", -1, 1)]
        [DataRow("Title", 0, 2)]
        [DataRow("Title", 100, 3)]
        public void Put_InvalidBookAndValidAuthorId_NotFoundResult(string title, int bookId, int authorId)
        {
            var book = new Book() { Title = title, Id = bookId };
            var expected = (int)HttpStatusCode.NotFound; ;
            book.AuthorId = authorId;
            NotFoundResult result = (NotFoundResult)controller.Put(book);
            var actual = result.StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("Title", 1, 1)]
        public void Put_ValidBookAndValidAuthorId_OkObjectResult(string title, int bookId, int authorId)
        {
            var book = new Book() { Title = title, Id = bookId };
            var expected = (Book)(new JsonResult(book).Value);
            book.AuthorId = authorId;
            IActionResult result = controller.Put(book);
            var actual = (Book)(((JsonResult)result).Value);

            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Title, actual.Title);
        }

        [TestMethod]
        [DataRow("Title", 1)]
        [DataRow("", 2)]
        public void Post_ValidBookAndValidAuthorId_CreatedAtAction(string title, int authorId)
        {
            Book book = new Book() { Title = title };
            var author = fakeAuthorManager.GetAuthor(authorId);
            var expected = new CreatedAtActionResult("Get", "Authors", new object(), author);

            var result = (CreatedAtActionResult)controller.Post(book, authorId);
            var actualBook = (Book)(result.Value);

            Assert.AreEqual(title, actualBook.Title);
            Assert.AreEqual(expected.ActionName, (result).ActionName);
        }

        [TestMethod]
        [DataRow(1, -1)]
        [DataRow(1, 0)]
        [DataRow(1, 100)]
        public void Delete_ValidAuthorIdAndInvalidBookId_NotFoundResult(int authorId, int invalidBookId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Delete(authorId, invalidBookId);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(0, 1)]
        [DataRow(-1, 1)]
        [DataRow(100, 1)]
        public void Delete_InvalidAuthorIdAndValidBookId_NotFoundResult(int invalidAuthorId, int bookId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Delete(invalidAuthorId, bookId);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1, 1)]
        [DataRow(1, 3)]
        [DataRow(2, 4)]
        [DataRow(3, 5)]
        public void Delete_ValidAuthorIdAndValidBookId_OkResult(int authorId, int bookId)
        {
            var expected = (int)HttpStatusCode.OK; ;

            IActionResult result = controller.Delete(authorId, bookId);
            var actual = ((OkResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

    }
}


