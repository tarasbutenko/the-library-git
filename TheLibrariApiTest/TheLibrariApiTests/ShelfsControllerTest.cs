﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TheLibraryApi.Controllers;

namespace TheLibrariApiTest
{
    [TestClass]
    public class ShelfsControllerTest
    {
        private IAuthorManager fakeAuthorManager;
        private ShelfsController controller;

        [TestInitialize]
        public void StartUp()
        {
            fakeAuthorManager = new FakeAuthorManager();
            controller = new ShelfsController(fakeAuthorManager);
        }
        [TestMethod]
        public void Get_Default_OkObjectResultWithShelfsCollection()
        {
            var expected = fakeAuthorManager.GetShelfsCollection();

            var result = (OkObjectResult)controller.Get();
            CollectionAssert.AreEqual(expected, (List<Shelf>)result.Value);
        }

        [TestMethod]
        [DataRow(1)]
        public void Get_ValidShelfId_OkObjectResultWithShelf(int shelfId)
        {
            var expected = fakeAuthorManager.GetShelf(shelfId);

            var result = (OkObjectResult)controller.Get(shelfId);

            Assert.AreEqual(expected, (result).Value);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(100)]
        public void Get_InvalidShelfId_NotFoundResult(int fakeShelfId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            var result = (NotFoundResult)controller.Get(fakeShelfId);
            var actual = result.StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("Name", 1)]
        public void Put_ValidShelf_CreatedAtAction(string Name, int shelfId)
        {
            var shelf = fakeAuthorManager.GetShelf(shelfId);
            shelf.Name = Name;
            var expected = new OkObjectResult(shelf);

            OkObjectResult result = (OkObjectResult)controller.Put(shelf);
            var actualShelf = (Shelf)(result.Value);

            Assert.AreEqual(Name, actualShelf.Name);
        }

        [TestMethod]
        [DataRow("name", -1)]
        [DataRow("name", 0)]
        [DataRow("name", 100)]
        public void Put_InvalidShelf_NotFoundResult(string Name, int shelfId)
        {
            var shelf = new Shelf() { Name = Name, Id = shelfId };
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Put(shelf);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("Name")]
        public void Post_ValidShelf_CreatedAtAction(string Name)
        {
            var shelf = new Shelf() { Name = Name };
            var expected = (int)HttpStatusCode.OK;

            var result = (OkResult)controller.Post(shelf);
            var actual = result.StatusCode;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(100)]
        [DataRow(0)]
        public void Delete_InvalidShelfId_NotFoundResult(int fakeShelfId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Delete(fakeShelfId);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1)]
        public void Delete_ValidShelfId_OkResult(int shelfId)
        {
            var expected = (int)HttpStatusCode.OK;

            IActionResult result = controller.Delete(shelfId);
            var actual = ((OkResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

    }
}
