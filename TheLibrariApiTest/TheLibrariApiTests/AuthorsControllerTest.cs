﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheLibraryApi.Controllers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace TheLibrariApiTest
{
    [TestClass]
    public class AuthorsControllerTest
    {
        private IAuthorManager fakeAuthorManager;
        private AuthorsController controller;

        [TestInitialize]
        public void StartUp()
        {
            fakeAuthorManager = new FakeAuthorManager();
            controller = new AuthorsController(fakeAuthorManager);
        }

        [TestMethod]
        public void Get_Default_OkObjectResultWithAuthorsCollection()
        {
            var expected = fakeAuthorManager.GetAuthorCollection();
            var expectedObject = (List<Author>)new JsonResult(expected).Value;

            var result = (JsonResult)controller.Get();
            CollectionAssert.AreEqual(expectedObject, (List<Author>)result.Value);
        }

        [TestMethod]
        [DataRow(3)]
        [DataRow(1)]
        public void Get_ValidAuthorId_OkObjectResultWithAuthor(int authorId)
        {
            var expected = fakeAuthorManager.GetAuthor(authorId);

            var result = (JsonResult)controller.Get(authorId);

            Assert.AreEqual(expected, (result).Value);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(100)]
        public void Get_InvalidAuthorId_NotFoundResult(int fakeAuthorId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            var result = (NotFoundResult)controller.Get(fakeAuthorId);
            var actual = (result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("first", "last", 1)]
        public void Put_ValidAuthor_CreatedAtAction(string firstName, string lastName, int authorId)
        {
            var author = new Author() { FirstName = firstName, LastName = lastName, Id = authorId };
            var expected = new JsonResult(author);

            var result = (JsonResult)controller.Put(author);
            var actualAuthor = (Author)(result.Value);

            Assert.AreEqual(firstName, actualAuthor.FirstName);
            Assert.AreEqual(lastName, actualAuthor.LastName);
        }

        [TestMethod]
        [DataRow("first", "last", -1)]
        [DataRow("first", "last", 0)]
        [DataRow("first", "last", 100)]
        public void Put_InvalidAuthor_NotFoundResult(string firstName, string lastName, int invalidAuthorId)
        {
            var author = new Author() { FirstName = firstName, LastName = lastName, Id = invalidAuthorId };
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Put(author);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("first", "last")]
        public void Post_ValidAuthor_CreatedAtAction(string firstName, string lastName)
        {

            var author = new Author() { FirstName = firstName, LastName = lastName };
            var expected = new CreatedAtActionResult("Get", "Authors", new object(), author);

            var result = (CreatedAtActionResult)controller.Post(author);
            var actualAuthor = (Author)(result.Value);

            Assert.AreEqual(firstName, actualAuthor.FirstName);
            Assert.AreEqual(lastName, actualAuthor.LastName);
            Assert.AreEqual(expected.ActionName, (result).ActionName);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(100)]
        [DataRow(0)]
        public void Delete_InvalidBookId_NotFoundResult(int invalidBookId)
        {
            var expected = (int)HttpStatusCode.NotFound;

            IActionResult result = controller.Delete(invalidBookId);
            var actual = ((NotFoundResult)result).StatusCode;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1)]
        public void Delete_ValidAuthorId_OkResult(int authorId)
        {
            var expected = (int)HttpStatusCode.OK; ;

            IActionResult result = controller.Delete(authorId);
            var actual = ((OkResult)result).StatusCode;

            Assert.AreEqual(expected, actual);

        }
    }
}
