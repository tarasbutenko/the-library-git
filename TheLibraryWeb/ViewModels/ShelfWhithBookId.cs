﻿using Models;

namespace ViewModels
{
    public class ShelfWithBookId
    {
        public Shelf Shelf;
        public int BookId;
    }
}
