﻿using Models;
using System.Collections.Generic;

namespace ViewModels
{
    public class ShelfCollectionWithBookId
    {
        public int BookId;
        public List<Shelf> Shelfs;
    }
}
