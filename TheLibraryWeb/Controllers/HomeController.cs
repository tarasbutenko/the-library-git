﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models;
using ViewModels;

namespace TheLibraryAspNetCore.Controllers
{
    public class HomeController : Controller
    {
        internal IAuthorManager authorManager;
        public HomeController(IAuthorManager manager)
        {

            authorManager = manager;
            authorManager.Initialize();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet("[controller]/DeleteBook/{bookId}", Name = "DeleteBook")]
        public ActionResult DeleteBook(int bookId)
        {
            var book = authorManager.GetBook(bookId);
            return View("DeleteBook", book);
        }

        [HttpPost("[controller]/DeleteBook/{Id}")]
        public ActionResult DeleteBook(Book book)
        {
            if (authorManager.GetBook(book.Id) != null)
            {
                authorManager.DeleteBook(book.Id);
                return RedirectToAction("Index", authorManager.GetAuthorCollection());
            }
            return View("Trouble");
        }


        public ActionResult Index()
        {
            return View(authorManager.GetAuthorCollection());
        }

        public ActionResult ViewBooks(int authorId)
        {

            var author = authorManager.GetAuthor(authorId);
            if (author == null)
            {
                return View("Trouble");
            }
            return View("ViewBooks", author);
        }

        [HttpGet]
        public ActionResult AddBook(int authorId)
        {
            if (authorManager.GetAuthor(authorId) != null)
            {
                return View("AddBook", new Book() { AuthorId = authorId });
            }
            return View("Trouble");
        }
        public ActionResult Trouble()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBook(Book book)
        {
            var author = authorManager.GetAuthor(book.AuthorId);
            if (author != null)
            {
                authorManager.InsertBook(book);
                return RedirectToAction("Index");
            }
            else
            {
                return View("Trouble");
            }
        }

        public ActionResult ViewShelfs(int bookId)
        {
            var book = authorManager.GetBook(bookId);
            if (book == null)
            {
                return View("Trouble");
            }
            var shelfs = authorManager.GetShelfsCollectionByBookId(bookId);
            var viewModel = new ShelfCollectionWithBookId() { BookId = bookId, Shelfs = shelfs };
            return View(viewModel);
        }

        public ActionResult ViewAllShelfs()
        {
            return View(authorManager.GetShelfsCollection());
        }

        [HttpGet("[controller]/DeleteShelf/{Id}")]
        public ActionResult DeleteShelf(Shelf shelf)
        {
            shelf = authorManager.GetShelf(shelf.Id);
            if (shelf != null)
            {
                return View("DeleteShelf", shelf);
            }
            return View("Trouble");
        }

        [HttpPost("[controller]/DeleteShelf/{shelfId}")]
        public ActionResult DeleteShelf(int shelfId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            if (shelf == null)
            {
                return RedirectToAction("Trouble");
            }
            authorManager.DeleteShelf(shelfId);
            return RedirectToAction("ViewAllShelfs");
        }

        [HttpGet]
        public ActionResult CreateShelf()
        {

            return View("CreateShelf");
        }

        [HttpPost]
        public ActionResult CreateShelf(Shelf shelf)
        {
            authorManager.InsertShelf(shelf);
            return RedirectToAction("ViewAllShelfs");
        }

        [HttpGet("[controller]/AddShelf/{bookId}")]
        public ActionResult AddShelf(int bookId)
        {
            var book = authorManager.GetBook(bookId);
            if (book == null)
            {
                return View("Trouble");
            }
            var allShelfs = authorManager.GetShelfsCollection();
            var shelfs = authorManager.GetShelfsCollectionByBookId(bookId);
            foreach (var shelf in shelfs)
            {
                allShelfs.Remove(shelf);
            }
            var viewModel = new ShelfCollectionWithBookId() { BookId = bookId, Shelfs = allShelfs };
            return View("AddShelf", viewModel);
        }
        [HttpPost]
        public ActionResult AddShelf(int bookId, Shelf shelf)

        {
            var book = authorManager.GetBook(bookId);
            shelf = authorManager.GetShelf(shelf.Id);
            if (book == null || shelf == null)
            {
                return View("Trouble");
            }
            authorManager.AddNewShelfToBook(shelf, book);
            return RedirectToAction("Index");
        }
        [HttpGet("[controller]/RemoveShelf/{bookId}/{shelfId}")]
        public ActionResult RemoveShelf(int shelfId, int bookId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            var book = authorManager.GetBook(bookId);
            if (book == null || shelf == null)
            {
                return View("Trouble");
            }
            var shelfWithBookId = new ShelfWithBookId() { BookId = bookId, Shelf = shelf };
            return View("RemoveShelf", shelfWithBookId);
        }
        [HttpPost("[controller]/RemoveShelf/{bookId}/{Id}")]
        public ActionResult RemoveShelf(Shelf shelf, int bookId)
        {
            shelf = authorManager.GetShelf(shelf.Id);
            var book = authorManager.GetBook(bookId);
            if (book == null || shelf == null)
            {
                return RedirectToAction("Trouble");
            }
            authorManager.RemoveShelfFromBook(shelf, book);
            return RedirectToAction("Index");
        }
    }
}
