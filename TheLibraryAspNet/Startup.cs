﻿using System;
using System.Collections.Generic;
using StructureMap;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace TheLibraryAspNet
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        // DI with Autofac
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc().AddControllersAsServices();

            var container = new Container();
            container.Configure(config =>
            {
                config.For<IAuthorManager>().Use<AuthorManager>();
                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }
                
        /*
        // DI with standart methods
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IAuthorManager, AuthorManager>();

            // Add framework services.
            services.AddMvc();
}
        */
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")),
                RequestPath = "/node_modules"
            });
        }
    }
}
