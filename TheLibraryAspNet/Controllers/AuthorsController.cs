﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;

namespace TheLibraryApi.Controllers
{
    [Route("api/[controller]")]
    public class AuthorsController : Controller
    {
        private JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.Indented,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };
        public IAuthorManager authorManager;

        public AuthorsController(IAuthorManager manager)
        {
            authorManager = manager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var authorCollection = authorManager.GetAuthorCollection();
            return base.Json(authorCollection, settings);
        }

        [HttpGet("{authorId}")]
        public IActionResult Get(int authorId)
        {
            var author = authorManager.GetAuthor(authorId);
            if (author != null)
            {
                return base.Json(author, settings);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        [HttpPost]
        public IActionResult Post(Author author)
        {
            return CreatedAtAction("Get", authorManager.InsertAuthor(author));
        }

        [HttpPut("{id}")]
        public IActionResult Put(Author author)
        {
            var newAuthor = authorManager.GetAuthor(author.Id);
            if (newAuthor != null)
            {
                authorManager.UpdateAuthor(author);
                return base.Json(author, settings);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        [HttpDelete("{authorId}")]
        public IActionResult Delete(int authorId)
        {
            var author = authorManager.GetAuthor(authorId);
            if (author != null)
            {
                authorManager.DeleteAuthor(authorId);
                return Ok();
            }
            else
            {
                return new NotFoundResult();
            }
        }
    }
}
