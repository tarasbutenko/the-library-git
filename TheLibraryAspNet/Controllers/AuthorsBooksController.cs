using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;

namespace TheLibraryApi.Controllers
{
    [Produces("application/json")]
    [Route("api/authors/{authorId?}/books")]
    public class AuthorsBooksController : Controller
    {
        private JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.Indented,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public IAuthorManager authorManager;

        public AuthorsBooksController(IAuthorManager manager)
        {
            authorManager = manager;
        }

        [HttpGet]
        public IActionResult Get(int authorId)
        {
            var author = authorManager.GetAuthor(authorId);
            if (author != null)
            {
                return base.Json(author.Books, settings);
            }
            else
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("{bookId}")]
        public IActionResult Get(int bookId, int authorId)
        {
            var author = authorManager.GetAuthor(authorId);
            Book book;
            if (author == null || !author.Books.Any(b => b.Id == bookId))
            {
                return new NotFoundResult();
            }
            else
            {
                book = author.Books.FirstOrDefault(b => b.Id == bookId);
            }
            if (book == null)
            {
                return new NotFoundResult();
            }
            else
            {
                return base.Json(book, settings);

            }
        }

        [HttpPost]
        public IActionResult Post(Book book, int authorId)
        {
            var author = authorManager.GetAuthor(authorId);
            if (author == null)
            {
                return new NotFoundResult();
            }
            book.AuthorId = authorId;
            authorManager.InsertBook(book);
            return CreatedAtAction("Get", authorManager.GetBook(book.Id));
        }

        [HttpPut("{Id}")]
        public IActionResult Put(Book book)
        {
            var OldBook = authorManager.GetBook(book.Id);
            if (OldBook == null)
            {
                return new NotFoundResult();
            }
            else
            {
                authorManager.UpdateBook(book);
                return base.Json(book, settings);
            }
        }

        [HttpDelete("{bookId}")]
        public IActionResult Delete(int authorId, int bookId)
        {
            if ((authorManager.GetAuthor(authorId) != null) && (authorManager.GetBook(bookId) != null) && (authorManager.GetBook(bookId).AuthorId == authorId))
            {
                if (authorManager.DeleteBook(bookId))
                {
                    return Ok();
                }

            }
            return new NotFoundResult();

        }
    }
}
