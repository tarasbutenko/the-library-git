using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TheLibraryApi.Controllers
{
    [Route("app")]
    public class MainController : Controller
    {
        public IActionResult Index()
        {
            return PartialView("Index");
        }
    }
}