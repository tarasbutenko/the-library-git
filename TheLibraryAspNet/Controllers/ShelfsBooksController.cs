using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;

namespace TheLibraryApi.Controllers
{
    [Produces("application/json")]
    [Route("api/shelfs/{shelfId}/books/")]
    //[Route("api/ShelfsBooksAuthor")]
    public class ShelfsBooksController : Controller
    {
        private JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.Indented,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        IAuthorManager authorManager;
        public ShelfsBooksController(IAuthorManager manager)
        {
            authorManager = manager;
        }

        [HttpGet]
        public IActionResult Get(int shelfId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            if (shelf != null)
            {
                var booksCollection = authorManager.GetBooksCollectionByShelfId(shelfId);

                return base.Json(booksCollection, settings);
            }
            return new NotFoundResult();

        }

        [HttpGet("{bookId}")]
        public IActionResult Get(int shelfId, int bookId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            if (shelf == null)
            {
                return new NotFoundResult();
            }
            var book = authorManager.GetBooksCollectionByShelfId(shelfId).FirstOrDefault(b => b.Id == bookId);
            if (book == null)
            {
                return new NotFoundResult();
            }
            return base.Json(book,settings);
        }

        [HttpPost(Name = "Post")]
        public IActionResult CreateRelationship(int shelfId, int bookId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            var book = authorManager.GetBook(bookId);
            if (shelf != null && book != null && !book.ShelfsBooks.Any(s => s.ShelfId == shelfId))
            {
                authorManager.AddNewShelfToBook(shelf, book);
                return base.Json(book, settings);
            }
            return new NotFoundResult();
        }

        [HttpPut("{Id}")]
        public IActionResult Put(Book book)
        {
            var OldBook = authorManager.GetBook(book.Id);
            if (OldBook == null)
            {
                return new NotFoundResult();
            }
            else
            {
                authorManager.UpdateBook(book);
                return base.Json(book, settings);
            }
        }

        [HttpDelete("{bookId}", Name = "Delete")]
        public IActionResult Remove(int shelfId, int bookId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            var book = authorManager.GetBook(bookId);
            authorManager.RemoveShelfFromBook(shelf, book);
            return Ok();
        }
    }
}
