using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace TheLibraryApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ShelfsController : Controller
    {

        IAuthorManager authorManager;
        public ShelfsController(IAuthorManager manager)
        {
            authorManager = manager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(authorManager.GetShelfsCollection());
        }

        [HttpGet("{shelfId}")]
        public IActionResult Get(int shelfId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            if (shelf == null)
            {
                return new NotFoundResult();
            }
            return Ok(shelf);
        }

        [HttpPost]
        public IActionResult Post(Shelf shelf)
        {
            authorManager.InsertShelf(shelf);
            return Ok();
        }

        [HttpPut("{Id}")]
        public IActionResult Put(Shelf shelf)
        {
            var newShelf = authorManager.GetShelf(shelf.Id);
            if (newShelf != null)
            {
                newShelf.Name = shelf.Name;
                authorManager.UpdateShelf(newShelf);
                return Ok(authorManager.GetShelf(newShelf.Id));
            }
            return new NotFoundResult();
        }

        [HttpDelete("{shelfId}")]
        public IActionResult Delete(int shelfId)
        {
            var shelf = authorManager.GetShelf(shelfId);
            if (shelf == null)
            {
                return new NotFoundResult();
            }
            authorManager.DeleteShelf(shelfId);
            return Ok();
        }
    }
}
