﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class AuthorService {

    constructor(private http: Http) { }

    getData() {
        return this.http.get('http://localhost:57939/api/authors')
    }
}
