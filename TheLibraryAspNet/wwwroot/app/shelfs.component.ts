﻿import { Component, OnInit } from '@angular/core';
import { Shelf } from './shelf'
import { Response } from '@angular/http';
import { ShelfService } from './shelf.service'
import { HttpService } from './http.service';

@Component({
    selector: 'my-shelfs',
    templateUrl: './shelfs.component.html',
    providers: [ShelfService, HttpService]
})
export class ShelfsComponent implements OnInit {

    shelfs: Shelf[]=[];

    constructor(private httpService: HttpService) { }

    ngOnInit(): void {
        this.httpService.getData().subscribe((data: Response) => {
            let shelfList = data.json();
            for (let index in shelfList)
            {
                console.log(shelfList[index]);
                let shelf = shelfList[index];
                this.shelfs.push({Id:shelf.id, Name:shelf.name});
            }
        });
    }
}