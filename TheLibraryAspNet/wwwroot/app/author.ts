﻿import {Book} from './book'
export class Author {
    Id: number;
    FirstName: string;
    LastName: string;
    Books:Book[]
}