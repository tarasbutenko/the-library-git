import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AuthorViewComponent } from './author-view';
import { AuthorsComponent } from './authors.component';
import { ShelfsComponent } from './shelfs.component';
import { ShelfViewComponent } from './shelf-view';
import { HttpModule } from '@angular/http';

const appRoutes: Routes = [
    {
        path: 'authors',
        component: AuthorsComponent
    }, {
        path: 'shelfs',
        component: ShelfsComponent
    }, {
        path: 'shelfs/:Id',
        component: ShelfViewComponent
    }, {
        path: '',
        redirectTo: '/app',
        pathMatch: 'full'
    }
];
@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule.forRoot(appRoutes), HttpModule],
    declarations: [AppComponent, AuthorsComponent, AuthorViewComponent, ShelfsComponent, ShelfViewComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
