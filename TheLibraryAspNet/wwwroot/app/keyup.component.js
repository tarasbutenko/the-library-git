"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var KeyUpComponent = (function () {
    function KeyUpComponent() {
        this.value = '';
    }
    KeyUpComponent.prototype.onEnter = function (value) { this.value = value; };
    return KeyUpComponent;
}());
KeyUpComponent = __decorate([
    core_1.Component({
        selector: 'keyup',
        template: "\n    <input #box (keyup.enter)=\"onEnter(box.value)\">\n    <p>{{value}}</p>\n  "
    })
], KeyUpComponent);
exports.KeyUpComponent = KeyUpComponent;
//# sourceMappingURL=keyup.component.js.map