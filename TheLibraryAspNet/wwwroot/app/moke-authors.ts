﻿import { Author } from './author'
import { Book } from './book'
export const AUTHORS: Author[] = [
    { Id: 11, LastName:'', Books:[] , FirstName: 'Mr. Nice' },
    { Id: 12, LastName:'', Books:[] , FirstName: 'Narco' },
    { Id: 13, LastName:'', Books:[] , FirstName: 'Bombasto' },
    { Id: 14, LastName:'', Books:[] , FirstName: 'Celeritas' },
    { Id: 15, LastName:'', Books:[] , FirstName: 'Magneta' },
    { Id: 16, LastName:'', Books:[] , FirstName: 'RubberMan' },
    { Id: 17, LastName:'', Books:[] , FirstName: 'Dynama' },
    { Id: 18, LastName:'', Books:[] , FirstName: 'Dr IQ' },
    { Id: 19, LastName:'', Books:[] , FirstName: 'Magma' },
    { Id: 20, LastName:'', Books:[] , FirstName: 'Tornado' }
];                                      
