﻿import { Component } from '@angular/core';

@Component({
    selector: 'keyup',
    template: `
    <input #box (keyup.enter)="onEnter(box.value)">
    <p>{{value}}</p>
  `
})
export class KeyUpComponent {
    value = '';
    onEnter(value: string) { this.value = value; }
}
