﻿import { Component, Input } from '@angular/core';
import { Author } from './author'
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'author-view',
    template: `
    <div *ngIf="author">
    <br/>
    <label>id:</label>{{author.Id }}
    <br/>    
    <label>first name:</label>{{ author.FirstName }}
     <br/>   
    <label>last name:</label>{{ author.LastName }}
     <br/>  
    First name:<input [(ngModel)] = "author.FirstName">
     <br/>  
    Last name:<input [(ngModel)] = " author.LastName" >
     <div *ngFor="let book of author.Books">
        book id:{{book.Id }}
        author id:{{book.AuthorId }}
        title:{{ book.Title }}
    </div>
    <br/>
`

})
export class AuthorViewComponent {
    @Input() author: Author;

}