"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var author_1 = require("./author");
var AuthorViewComponent = (function () {
    function AuthorViewComponent() {
    }
    return AuthorViewComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", author_1.Author)
], AuthorViewComponent.prototype, "author", void 0);
AuthorViewComponent = __decorate([
    core_1.Component({
        selector: 'author-view',
        template: "\n    <div *ngIf=\"author\">\n    <br/>\n    <label>id:</label>{{author.Id }}\n    <br/>    \n    <label>first name:</label>{{ author.FirstName }}\n     <br/>   \n    <label>last name:</label>{{ author.LastName }}\n     <br/>  \n    First name:<input [(ngModel)] = \"author.FirstName\">\n     <br/>  \n    Last name:<input [(ngModel)] = \" author.LastName\" >\n     <div *ngFor=\"let book of author.Books\">\n        book id:{{book.Id }}\n        author id:{{book.AuthorId }}\n        title:{{ book.Title }}\n    </div>\n    <br/>\n"
    })
], AuthorViewComponent);
exports.AuthorViewComponent = AuthorViewComponent;
//# sourceMappingURL=author-view.js.map