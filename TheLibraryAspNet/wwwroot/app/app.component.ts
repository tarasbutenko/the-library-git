import { Component } from '@angular/core'
import { AuthorsComponent } from './authors.component'
import { ShelfsComponent } from './shelfs.component'
@Component({
    selector: 'my-app',
    template: `
<div>
    {{title}}
<h2>Find you book by author or shelf</h2>
    <label>Chose you destiny </label>
    <a routerLink="/authors">Authors</a>
    <router-outlet> </router-outlet>
    <a routerLink="/shelfs" > Shelfs </a>
    <router-outlet> </router-outlet>
</div>`
})
export class AppComponent {
    title: "Taras's app";
}