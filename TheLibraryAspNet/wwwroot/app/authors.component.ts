﻿import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Author } from './author'
import { AuthorService } from './author.service'
import { Response } from '@angular/http';

@Component({
    selector: 'my-authors',
    template: `
<h1>Hello {{name}}</h1>
<div>
<h2>Authors</h2>
 <div 
    *ngFor="let author of authors"
    (click)="onSelect(author)">
     <a routerLink="/authors/{{author.Id }}" routerLinkActive="active"> 
        {{ author.FirstName }}
        {{ author.LastName }}
    </a>
      <router-outlet></router-outlet>
</div>
<author-view [author] = "chosenAuthor"></author-view>
`,
    styles: [`
  .selected {
    background-color: #CFD8DC !important;
    color: white;
  }
  .authors {
    margin: 0 0 2em 0;
    list-style-type: none;
    padding: 0;
    width: 15em;
  }
  .authors li {
    cursor: pointer;
    position: relative;
    left: 0;
    background-color: #EEE;
    margin: .5em;
    padding: .3em 0;
    height: 1.6em;
    border-radius: 4px;
  }
  .authors li.selected:hover {
    background-color: #BBD8DC !important;
    color: white;
  }
  .authors li:hover {
    color: #607D8B;
    background-color: #DDD;
    left: .1em;
  }
  .authors .text {
    position: relative;
    top: -3px;
  }
  .authors .badge {
    display: inline-block;
    font-size: small;
    color: white;
    padding: 0.8em 0.7em 0 0.7em;
    background-color: #607D8B;
    line-height: 1em;
    position: relative;
    left: -1px;
    top: -4px;
    height: 1.8em;
    margin-right: .8em;
    border-radius: 4px 0 0 4px;
  }
`],
    providers: [AuthorService]
})
export class AuthorsComponent implements OnInit {
    name = 'Taras';
    chosenAuthor: Author;
    authors: Author[]=[];
    onSelect(chosenAuthor: Author): void { this.chosenAuthor = chosenAuthor; };
    constructor(private authorService: AuthorService) { };

    ngOnInit(): void {
        this.authorService.getData().subscribe((data: Response) => {
            let authorList = data.json();
            for (let index in authorList) {
                console.log(authorList[index]);
                let author = authorList[index];
                this.authors.push(
                    {
                        FirstName: author.FirstName,
                        LastName: author.LastName,
                        Id: author.Id,
                       Books:author.Books
                    });
            }
        });
    }
}