"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var shelf_service_1 = require("./shelf.service");
var http_service_1 = require("./http.service");
var ShelfsComponent = (function () {
    function ShelfsComponent(httpService) {
        this.httpService = httpService;
        this.shelfs = [];
    }
    ShelfsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpService.getData().subscribe(function (data) {
            var shelfList = data.json();
            for (var index in shelfList) {
                console.log(shelfList[index]);
                var shelf = shelfList[index];
                _this.shelfs.push({ Id: shelf.id, Name: shelf.name });
            }
        });
    };
    return ShelfsComponent;
}());
ShelfsComponent = __decorate([
    core_1.Component({
        selector: 'my-shelfs',
        templateUrl: './shelfs.component.html',
        providers: [shelf_service_1.ShelfService, http_service_1.HttpService]
    }),
    __metadata("design:paramtypes", [http_service_1.HttpService])
], ShelfsComponent);
exports.ShelfsComponent = ShelfsComponent;
//# sourceMappingURL=shelfs.component.js.map