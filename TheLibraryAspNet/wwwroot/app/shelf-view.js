"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/switchMap");
var ShelfViewComponent = (function () {
    function ShelfViewComponent() {
    }
    ShelfViewComponent.prototype.ngOnInit = function () {
        throw new Error("Method not implemented.");
    };
    return ShelfViewComponent;
}());
ShelfViewComponent = __decorate([
    core_1.Component({
        selector: 'shelf-view',
        template: "\n    <div *ngIf=\"shelf\">\n    <br/>\n    <label>Id:</label>{{shelf.Id }}\n    <br/>    \n    <label>shelf:</label>{{ shelf.Name}}\n    <br/>  \n    New name:<input [(ngModel)] = \"shelf.Name\">\n    </div> \n    <br/>\n", providers: []
    })
], ShelfViewComponent);
exports.ShelfViewComponent = ShelfViewComponent;
//# sourceMappingURL=shelf-view.js.map