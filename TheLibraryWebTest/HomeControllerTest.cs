﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLibraryAspNetCore.Controllers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Models;
using Moq;
using ViewModels;

namespace TheLibraryWebTest
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index_Default_AuthorCollection()
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthorCollection()).Returns(new List<Author>() { new Author(), new Author() });
            HomeController controller = new HomeController(mock.Object);
            var expectedAuthorCollections = mock.Object.GetAuthorCollection();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedAuthorCollections, result.Model);
            mock.Verify(am => am.GetAuthorCollection());
        }

        [TestMethod]
        public void About_Default_NotNullResult()
        {
            // Arrange
            HomeController controller = new HomeController(new Mock<IAuthorManager>().Object);

            // Act
            var result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Contact_Default_NotNullViewResult()
        {
            // Arrange
            HomeController controller = new HomeController(new Mock<IAuthorManager>().Object);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        [DataRow(1)]
        [DataRow(7)]
        public void DeleteBook_BookId_ViewResult(int bookId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            var expectedObject = new Book() { Id = bookId, Title = "title" };
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(expectedObject);
            HomeController controller = new HomeController(mock.Object);
            var expectedName = "DeleteBook";

            // Act
            ViewResult result = controller.DeleteBook(bookId) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedName, result.ViewName);
            Assert.AreEqual(expectedObject, result.Model);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void DeleteBook_InvalidBook_ViewResult(int invalidBookId, string title)
        {
            // Arrange
            var book = new Book() { Id = 1, AuthorId = invalidBookId, Title = title };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            // Act
            var result = (ViewResult)controller.DeleteBook(book);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void DeleteBook_ValidBook_RedirectToActionResult(int validBookId, string title)
        {
            // Arrange
            var book = new Book() { Id = 1, AuthorId = validBookId, Title = title };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == validBookId))).Returns(book);
            HomeController controller = StartUp(mock.Object);
            var expected = "Index";

            // Act
            var result = (RedirectToActionResult)controller.DeleteBook(book);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == validBookId)));
            mock.Verify(am => am.DeleteBook(It.Is<int>(i => i == validBookId)));
        }

        [TestMethod]
        [DataRow(0)]
        public void ViewBooks_InvalidAuthorId_ViewResultTrouble(int invalidAuthorId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i != invalidAuthorId))).Returns((Author)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.ViewBooks(invalidAuthorId);

            //Assert
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == invalidAuthorId)));
        }

        [TestMethod]
        [DataRow(1)]
        public void ViewBooks_AuthorId_ViewResultWithAuthor(int authorId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            Author expectedAuthor = new Author() { Id = authorId };
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i == authorId))).Returns(expectedAuthor);
            HomeController controller = StartUp(mock.Object);
            var expected = "ViewBooks";

            //Act
            var result = (ViewResult)controller.ViewBooks(authorId);

            //Assert
            Assert.AreEqual(expected, result.ViewName);
            Assert.AreEqual(expectedAuthor, result.Model);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == authorId)));
        }


        [TestMethod]
        [DataRow(1)]
        public void AddBookGet_AuthorId_ViewResult(int authorId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i == authorId))).Returns(new Author());
            HomeController controller = StartUp(mock.Object);
            var expectedViewName = "AddBook";

            //Act
            var result = (ViewResult)controller.AddBook(authorId);

            //Assert
            Assert.AreEqual(expectedViewName, result.ViewName);
            Assert.AreEqual(authorId, ((Book)result.Model).AuthorId);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == authorId)));
        }

        [TestMethod]
        [DataRow(1)]
        public void AddBookGet_InvalidAuthorId_ViewResult(int authorId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i == authorId))).Returns((Author)null);
            HomeController controller = StartUp(mock.Object);
            var expectedViewName = "Trouble";

            //Act
            var result = (ViewResult)controller.AddBook(authorId);

            //assert
            Assert.AreEqual(expectedViewName, result.ViewName);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == authorId)));
        }

        [TestMethod]
        [DataRow(1, "title", 1)]
        public void AddBookPost_BookWithInvalidAuthorId_ViewResult(int id, string title, int authorId)
        {
            // Arrange
            var book = new Book() { Id = id, Title = title, AuthorId = authorId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i == authorId))).Returns((Author)null);
            HomeController controller = StartUp(mock.Object);
            var expectedViewName = "Trouble";

            //Act
            var result = (ViewResult)controller.AddBook(book);

            //Assert
            Assert.AreEqual(expectedViewName, result.ViewName);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == authorId)));
        }

        [TestMethod]
        [DataRow(1, "title", 1)]
        public void AddBookPost_BookWithValidAuthorId_RedirectToActionResult(int id, string title, int authorId)
        {
            // Arrange
            var book = new Book() { Id = id, Title = title, AuthorId = authorId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetAuthor(It.Is<int>(i => i == authorId))).Returns(new Author());
            HomeController controller = StartUp(mock.Object);
            var expectedActionName = "Index";

            //Act
            var result = (RedirectToActionResult)controller.AddBook(book);

            //Assert
            Assert.AreEqual(expectedActionName, result.ActionName);
            mock.Verify(am => am.GetAuthor(It.Is<int>(i => i == authorId)));
            mock.Verify(am => am.InsertBook(It.Is<Book>(b => b == book)));
        }


        [TestMethod]
        public void Trouble_Default_IsNotNull()
        {
            // Arrange
            HomeController controller = new HomeController(new Mock<IAuthorManager>().Object);

            // Act
            ViewResult result = controller.Trouble() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [DataRow(1)]
        public void ViewShelfs_ValidBookId_ViewResultWithViewModel(int bookId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            var book = new Book() { Id = bookId };
            var shelfs = new List<Shelf>() { new Shelf(), new Shelf() };
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelfsCollectionByBookId(It.Is<int>(i => i == bookId))).Returns(shelfs);
            HomeController controller = StartUp(mock.Object);

            //Act
            var result = (ViewResult)controller.ViewShelfs(bookId);
            var expected = bookId;

            //Assert
            Assert.AreEqual(expected, ((ShelfCollectionWithBookId)result.Model).BookId);
            CollectionAssert.AreEqual(shelfs, ((ShelfCollectionWithBookId)result.Model).Shelfs);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelfsCollectionByBookId(It.Is<int>(i => i == bookId)));
        }

        [TestMethod]
        [DataRow(1)]
        public void ViewShelfs_InvalidBookId_ViewResult(int invalidBookId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            HomeController controller = StartUp(mock.Object);

            //Act
            var result = (ViewResult)controller.ViewShelfs(invalidBookId);
            var actual = result.ViewName;
            var expected = "Trouble";

            //Assert
            Assert.AreEqual(expected, actual);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
        }

        [TestMethod]
        public void ViewAllShelfs_Default_ViewResultWithModel()
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            var shelfs = new List<Shelf>() { new Shelf(), new Shelf() };
            mock.Setup(am => am.GetShelfsCollection()).Returns(shelfs);
            HomeController controller = StartUp(mock.Object);

            //Act
            var result = (ViewResult)controller.ViewAllShelfs();
            var actual = (List<Shelf>)result.Model;
            var expected = shelfs;

            //Assert
            CollectionAssert.AreEqual(expected, actual);
            mock.Verify(am => am.GetShelfsCollection());
        }

        [TestMethod()]
        [DataRow(1)]
        [DataRow(7)]
        public void DeleteShelfPost_ValidShelfId_RedirectToActionResult(int shelfId)
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            var shelf = new Shelf() { Id = shelfId, Name = "Name" };
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = new HomeController(mock.Object);
            var expectedName = "ViewAllShelfs";

            // Act
            var result = controller.DeleteShelf(shelfId) as RedirectToActionResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedName, result.ActionName);
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
            mock.Verify(am => am.DeleteShelf(It.Is<int>(i => i == shelfId)));
        }

        [TestMethod()]
        [DataRow(1)]
        [DataRow(7)]
        public void DeleteShelfPost_InvalidShelfId_RedirectToActionResult(int invalidShelfId)
        {
            // Arrange
            var shelf = new Shelf() { Id = invalidShelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId))).Returns((Shelf)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            // Act
            var result = (RedirectToActionResult)controller.DeleteShelf(invalidShelfId);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId)));
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void DeleteShelfGet_InvalidShelf_ViewResult(int invalidShelfId, string name)
        {
            // Arrange
            var shelf = new Shelf() { Id = invalidShelfId, Name = name };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId))).Returns((Shelf)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            // Act
            var result = (ViewResult)controller.DeleteShelf(shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId)));
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void DeleteShelfGet_ValidShelf_ViewResult(int validShelfId, string name)
        {
            // Arrange
            var shelf = new Shelf() { Id = validShelfId, Name = name };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == validShelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "DeleteShelf";

            // Act
            var result = (ViewResult)controller.DeleteShelf(shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            Assert.AreEqual(shelf, (Shelf)result.Model);
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == validShelfId)));
        }

        [TestMethod]
        public void CreateShelfGet_Defaulf_ViewResult()
        {
            // Arrange
            var mock = new Mock<IAuthorManager>();
            HomeController controller = StartUp(mock.Object);
            var expected = "CreateShelf";

            // Act
            var result = (ViewResult)controller.CreateShelf();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
        }

        [TestMethod()]
        public void CreateShelfPost_Shelf_RedirectToActionResult()
        {
            // Arrange
            var shelf = new Shelf();
            var mock = new Mock<IAuthorManager>();
            HomeController controller = StartUp(mock.Object);
            var expected = "ViewAllShelfs";

            // Act
            var result = (RedirectToActionResult)controller.CreateShelf(shelf);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.InsertShelf(It.Is<Shelf>(s => s == shelf)));
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void AddShelfGet_ValidBookId_ViewResultWithViewModel(int validBookId, string title)
        {
            // Arrange
            var book = new Book() { Id = validBookId, Title = title };
            var shelf = new Shelf() { Id = 1 };
            var shelfs = new List<Shelf>() { shelf };
            var allShelfs = new List<Shelf>() { new Shelf() { Id = 2 } };
            allShelfs.AddRange(shelfs);
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == validBookId))).Returns(book);
            mock.Setup(am => am.GetShelfsCollectionByBookId(It.Is<int>(i => i == validBookId))).Returns(shelfs);
            mock.Setup(am => am.GetShelfsCollection()).Returns(allShelfs);
            HomeController controller = StartUp(mock.Object);
            var expected = "AddShelf";

            // Act
            var result = (ViewResult)controller.AddShelf(validBookId);
            allShelfs.Remove(shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            Assert.AreEqual(validBookId, ((ShelfCollectionWithBookId)result.Model).BookId);
            CollectionAssert.AreEqual(allShelfs, ((ShelfCollectionWithBookId)result.Model).Shelfs);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == validBookId)));
            mock.Verify(am => am.GetShelfsCollectionByBookId(It.Is<int>(i => i == validBookId)));
            mock.Verify(am => am.GetShelfsCollection());
        }

        [TestMethod]
        [DataRow(1, "title")]
        public void AddShelfGet_InvalidBookId_ViewResult(int invalidBookId, string title)
        {
            //Arrange
            var book = new Book() { Id = invalidBookId, Title = title };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.AddShelf(invalidBookId);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
        }

        [TestMethod]
        [DataRow(1, 2)]
        public void AddShelfPost_InvalidBookIdAndValidShelf_ViewResult(int invalidBookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = invalidBookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.AddShelf(invalidBookId, shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void AddShelfPost_ValidBookIdAndInvalidShelf_ViewResult(int bookId, int invalidShelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = invalidShelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId))).Returns((Shelf)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.AddShelf(bookId, shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void AddShelfPost_ValidBookIdAndValidShelf_RedirectToActionResult(int bookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            mock.Setup(am => am.AddNewShelfToBook(It.Is<Shelf>(s => s == shelf), It.Is<Book>(b => b == book)));
            HomeController controller = StartUp(mock.Object);
            var expected = "Index";

            //Act
            var result = (RedirectToActionResult)controller.AddShelf(bookId, shelf);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
            mock.Verify(am => am.AddNewShelfToBook(It.Is<Shelf>(s => s == shelf), It.Is<Book>(b => b == book)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfGet_InvalidBookIdAndValidShelf_ViewResult(int invalidBookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = invalidBookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.RemoveShelf(invalidBookId, shelf.Id);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfGet_ValidBookIdAndInvalidShelf_ViewResult(int bookId, int invalidShelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = invalidShelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId))).Returns((Shelf)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (ViewResult)controller.RemoveShelf(bookId, shelf.Id);
            var actual = result.ViewName;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, actual);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfGet_ValidBookIdAndValidShelf_RedirectToActionResult(int bookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "RemoveShelf";

            //Act
            var result = (ViewResult)controller.RemoveShelf(bookId, shelf.Id);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
            Assert.AreEqual(bookId, ((ShelfWithBookId)result.Model).BookId);
            Assert.AreEqual(shelf, ((ShelfWithBookId)result.Model).Shelf);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfPost_InvalidBookIdAndValidShelf_ViewResult(int invalidBookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = invalidBookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == invalidBookId))).Returns((Book)null);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (RedirectToActionResult)controller.RemoveShelf(shelf, invalidBookId);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == invalidBookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfPost_ValidBookIdAndInvalidShelf_ViewResult(int bookId, int invalidShelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = invalidShelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId))).Returns((Shelf)null);
            HomeController controller = StartUp(mock.Object);
            var expected = "Trouble";

            //Act
            var result = (RedirectToActionResult)controller.RemoveShelf(shelf, bookId);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == invalidShelfId)));
        }

        [TestMethod]
        [DataRow(1, 1)]
        public void RemoveShelfPost_ValidBookIdAndValidShelf_RedirectToActionResult(int bookId, int shelfId)
        {
            //Arrange
            var book = new Book() { Id = bookId };
            var shelf = new Shelf() { Id = shelfId };
            var mock = new Mock<IAuthorManager>();
            mock.Setup(am => am.GetBook(It.Is<int>(i => i == bookId))).Returns(book);
            mock.Setup(am => am.GetShelf(It.Is<int>(i => i == shelfId))).Returns(shelf);
            HomeController controller = StartUp(mock.Object);
            var expected = "Index";

            //Act
            var result = (RedirectToActionResult)controller.RemoveShelf(shelf, bookId);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ActionName);
            mock.Verify(am => am.RemoveShelfFromBook(It.Is<Shelf>(s => s == shelf), It.Is<Book>(b => b == book)));
            mock.Verify(am => am.GetBook(It.Is<int>(i => i == bookId)));
            mock.Verify(am => am.GetShelf(It.Is<int>(i => i == shelfId)));
        }

        private HomeController StartUp(IAuthorManager manager)
        {
            return new HomeController(manager);
        }
    }
}
